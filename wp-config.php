<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', 'D:\ProgramWork\OpenServer\domains\resume.loc\wp-content\plugins\wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'resume');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%_^/LZS.12Vda!Y:P_9AGB%zFS}i%/qzM>q_xZ`$VQ]DAaO?E`Nne57]|kr:pL@m');
define('SECURE_AUTH_KEY',  'x`TEO+?O5r/VRIU4!kE^i/Gfq@vst._V:2Qy0-z-lEQ|p)%m-~X?{#dV1/EE>OS;');
define('LOGGED_IN_KEY',    '[oZZr*RWdAakLC=n2/n],,A:7]x%IH|.S[`[8rfI6+.@whc^=8}*wtW-:|wzdoGh');
define('NONCE_KEY',        '*x</YShU@9vK,hZsaBitxF#hrIMaPW2],:%U&|~4-n2t!5QXopKv;Au]ujOPzSpa');
define('AUTH_SALT',        'l4-pp6clM/Zu-3?bC}U=Q3o-S4D$L)y`bqA;<*4ZwmNr@#IgIYm&vQO,iBLqNmGo');
define('SECURE_AUTH_SALT', 'm|iuXi=[e}d.3KT2ne!+>UHE=>n{c5Ac;C?f+XZCBo4qYg(b59ci{ ArQG H6ICh');
define('LOGGED_IN_SALT',   '-Q3g>/kbO.mea]}VaM:di@;V7+vz>(r9Ob {{Tauu<!NmG-E?|AR9tdQi@M!K2]t');
define('NONCE_SALT',       ' 28P-OfC?ECpU]]z[*HR(X~[vs#^Xj@o#s/-Sgv5yt71$W>o+ukN>GWLtR*f>oSr');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
